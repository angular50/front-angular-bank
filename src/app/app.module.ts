import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminComponent } from './pages/admin/admin.component';
import { HttpClientModule } from '@angular/common/http';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ClientComponent } from './pages/client/client.component';
import { CompteComponent } from './pages/compte/compte.component';
import { RequeteComponent } from './pages/requete/requete.component';
import { VirementComponent } from './pages/virement/virement.component';
import { PartieDroiteClientComponent } from './components/partie-droite-client/partie-droite-client.component';


@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    HomePageComponent,
    ClientComponent,
    HeaderComponent,
    FooterComponent,
    ClientComponent,
    CompteComponent,
    RequeteComponent,
    VirementComponent,
    PartieDroiteClientComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
