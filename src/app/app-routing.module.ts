import { VirementComponent } from './pages/virement/virement.component';
import { RequeteComponent } from './pages/requete/requete.component';
import { CompteComponent } from './pages/compte/compte.component';
import { ClientComponent } from './pages/client/client.component';
import { HomePageComponent } from './pages/home-page/home-page.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './pages/admin/admin.component';
import { Client } from './models/client/client';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomePageComponent
      },
      {
        path: 'admin',
        component: AdminComponent
          },
          {
            path: 'client',
            component: ClientComponent
              },
              {
                path: 'client/:id',
                component: ClientComponent
              },
              {
                path: 'compte/:id',
                component: CompteComponent
              },
              {
                path: 'requete',
                component: RequeteComponent
                  },
                  {
                    path: 'virement',
                    component: VirementComponent
                      },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
