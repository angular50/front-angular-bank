import { Component, OnInit } from '@angular/core';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthentificationService } from 'src/app/services/authentification/authentification.service';
import { Router } from '@angular/router';
import { ClientHttpService } from 'src/app/services/client/client-http.service';
import { Client } from 'src/app/models/client/client';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  utilisateur: Utilisateur;
  client: Client;
  logForm: FormGroup;
  loggue: number;

  constructor(private fb: FormBuilder,
              private authService: AuthentificationService,
              private clientService: ClientHttpService,
              private router: Router) {
    this.logForm = fb.group({
      email: '',
      password: ''
   });
  }

  ngOnInit() {
    this.loggue = localStorage.length;
  }

  clearLocalStorage(){
    localStorage.clear();
  }

  login = () => {
    // tslint:disable-next-line: new-parens
    const utilisateur = new Utilisateur;
    utilisateur.email = this.logForm.value.email;
    utilisateur.mdp = this.logForm.value.password;

    this.authService.authentification(utilisateur).subscribe(
      // tslint:disable-next-line: no-shadowed-variable
      (utilisateur: Utilisateur) => {
        this.utilisateur = utilisateur;
        console.log(utilisateur);
        localStorage.setItem('idUtilisateur', JSON.stringify(utilisateur.idutilisateur));
        localStorage.setItem('email', utilisateur.email);
        localStorage.setItem('nom', utilisateur.nom);
        localStorage.setItem('prenom', utilisateur.prenom);
        if (localStorage.length === 0) {
          this.router.navigate(['/home']);
        } else {
          this.router.navigate(['/client', utilisateur.idutilisateur]);
        }

        // tslint:disable-next-line: radix
        this.clientService.getClientByIdUtilisateur(parseInt(localStorage.getItem('idUtilisateur'))).subscribe(
      (client: Client) => {
        this.client = client;
        localStorage.setItem('idClient', JSON.stringify(client.idclient));
        console.log(client); },
      err => {console.warn(err); }
    );
      },
      err => {console.warn(err); });

    }
}

