import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartieDroiteClientComponent } from './partie-droite-client.component';

describe('PartieDroiteClientComponent', () => {
  let component: PartieDroiteClientComponent;
  let fixture: ComponentFixture<PartieDroiteClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartieDroiteClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartieDroiteClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
