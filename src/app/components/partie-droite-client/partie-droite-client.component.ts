import { Component, OnInit } from '@angular/core';
import { Client } from 'src/app/models/client/client';
import { TypeRequete } from 'src/app/models/typeRequete/type-requete';
import { Compte } from 'src/app/models/compte/compte';
import { Transaction } from 'src/app/models/transaction/transaction';
import { ClientHttpService } from 'src/app/services/client/client-http.service';
import { TypeRequeteHttpService } from 'src/app/services/typeRequete/type-requete-http.service';
import { CompteHttpService } from 'src/app/services/compte/compte-http.service';
import { TransactionHttpService } from 'src/app/services/transaction/transaction-http.service';

@Component({
  selector: 'app-partie-droite-client',
  templateUrl: './partie-droite-client.component.html',
  styleUrls: ['./partie-droite-client.component.scss']
})
export class PartieDroiteClientComponent implements OnInit {
  client: Client;
  typeRequetes: TypeRequete[];
  comptes: Compte[];
  transactions: Transaction[];
  constructor(private clientService: ClientHttpService,
              private typeRequeteService: TypeRequeteHttpService,
              private compteService: CompteHttpService,
              private transactionService: TransactionHttpService
    ) { }

ngOnInit() {

// tslint:disable-next-line: radix
this.clientService.getClientByIdUtilisateur(parseInt(localStorage.getItem('idUtilisateur'))).subscribe(
(client: Client) => {
this.client = client;
console.log(client); },
err => {console.warn(err); }
);
this.typeRequeteService.getTypesRequetes().subscribe(
(typeRequetes: TypeRequete[]) => {
this.typeRequetes = typeRequetes;
console.log(typeRequetes); },
err => {console.warn(err); }
);
}

}

