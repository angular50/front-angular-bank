import { Component, OnInit } from '@angular/core';
import { TransactionHttpService } from 'src/app/services/transaction/transaction-http.service';
import { Transaction } from 'src/app/models/transaction/transaction';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-compte',
  templateUrl: './compte.component.html',
  styleUrls: ['./compte.component.scss']
})
export class CompteComponent implements OnInit {
  transactions: Transaction[];
  id: number;
  constructor(private transactionService: TransactionHttpService,
              private route: ActivatedRoute) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
      // tslint:disable-next-line: radix
      this.id = parseInt(params.id);
      localStorage.setItem('numCompte', JSON.stringify(this.id));
    });

    // tslint:disable-next-line: radix
    this.transactionService.getTransactionsByIdCompte(this.id).subscribe(
      (transactions: Transaction[]) => {
        this.transactions = transactions;
        console.log(transactions); },
      err => {console.warn(err); }
    );
  }

}
