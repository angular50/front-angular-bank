import { TypeRequeteHttpService } from './../../services/typeRequete/type-requete-http.service';
import { NotificationHttpService } from './../../services/notification/notification-http.service';
import { RequeteHttpService } from './../../services/requete/requete-http.service';
import { TransactionHttpService } from './../../services/transaction/transaction-http.service';
import { DemandeOuvertureHttpService } from './../../services/demandeOuverture/demande-ouverture-http.service';
import { CompteHttpService } from './../../services/compte/compte-http.service';
import { ClientHttpService } from './../../services/client/client-http.service';
import { Component, OnInit } from '@angular/core';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { UtilisateurHttpService } from 'src/app/services/utilisateur/utilisateur-http.service';
import { AgentHttpService } from 'src/app/services/agent/agent-http.service';

import { Agent } from 'src/app/models/agent/agent';
import { Client } from 'src/app/models/client/client';
import { Compte } from 'src/app/models/compte/compte';
import { DemandeOuverture } from 'src/app/models/demandeOuverture/demande-ouverture';
import { Transaction } from 'src/app/models/transaction/transaction';
import { Requete } from 'src/app/models/requete/requete';
import { Notif } from 'src/app/models/notif/notif';
import { TypeRequete } from 'src/app/models/typeRequete/type-requete';
import { TypeTransaction } from 'src/app/models/typeTransaction/type-transaction';
import { TypeTransactionHttpService } from 'src/app/services/typeTransaction/type-transaction-http.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  utilisateurs: Utilisateur[];
  agents: Agent[];
  clients: Client[];
  comptes: Compte[];
  demandes: DemandeOuverture[];
  transactions: Transaction[];
  requetes: Requete[];
  notifications: Notif[];
  typeRequetes: TypeRequete[];
  typeTransactions: TypeTransaction[];
  constructor(private utilisateurService: UtilisateurHttpService,
              private agentService: AgentHttpService,
              private clientService: ClientHttpService,
              private compteService: CompteHttpService,
              private demandeService: DemandeOuvertureHttpService,
              private transactionService: TransactionHttpService,
              private requeteService: RequeteHttpService,
              private notificationService: NotificationHttpService,
              private typeRequeteService: TypeRequeteHttpService,
              private typeTransactionService: TypeTransactionHttpService) { }

  ngOnInit() {
    this.utilisateurService.getUtilisateurs().subscribe(
      (utilisateurs: Utilisateur[]) => {
        this.utilisateurs = utilisateurs;
        console.log(utilisateurs); },
      err => {console.warn(err); }
    );
    this.agentService.getAgents().subscribe(
      (agents: Agent[]) => {
        this.agents = agents;
        console.log(agents); },
      err => {console.warn(err); }
    );
    this.clientService.getClients().subscribe(
      (clients: Client[]) => {
        this.clients = clients;
        console.log(clients); },
      err => {console.warn(err); }
    );
    this.compteService.getComptes().subscribe(
      (comptes: Compte[]) => {
        this.comptes = comptes;
        console.log(comptes); },
      err => {console.warn(err); }
    );
    this.demandeService.getDemandes().subscribe(
      (demandes: DemandeOuverture[]) => {
        this.demandes = demandes;
        console.log(demandes); },
      err => {console.warn(err); }
    );
    this.transactionService.getTransactions().subscribe(
      (transactions: Transaction[]) => {
        this.transactions = transactions;
        console.log(transactions); },
      err => {console.warn(err); }
    );
    this.requeteService.getRequetes().subscribe(
      (requetes: Requete[]) => {
        this.requetes = requetes;
        console.log(requetes); },
      err => {console.warn(err); }
    );
    this.notificationService.getNotifications().subscribe(
      (notifications: Notif[]) => {
        this.notifications = notifications;
        console.log(notifications); },
      err => {console.warn(err); }
    );
    this.typeRequeteService.getTypesRequetes().subscribe(
      (typeRequetes: TypeRequete[]) => {
        this.typeRequetes = typeRequetes;
        console.log(typeRequetes); },
      err => {console.warn(err); }
    );
    this.typeTransactionService.getTypesTransactions().subscribe(
      (typeTransactions: TypeTransaction[]) => {
        this.typeTransactions = typeTransactions;
        console.log(typeTransactions); },
      err => {console.warn(err); }
    );
  }

}
