import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Requete } from 'src/app/models/requete/requete';
import { RequeteHttpService } from 'src/app/services/requete/requete-http.service';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';

@Component({
  selector: 'app-requete',
  templateUrl: './requete.component.html',
  styleUrls: ['./requete.component.scss']
})
export class RequeteComponent implements OnInit {
  requeteForm: FormGroup;
  requete: Requete;
  constructor(private fb: FormBuilder,
              private requeteService: RequeteHttpService, ) {
    this.requeteForm = fb.group({
      message: ''
    });
   }

  ngOnInit() {
  }

  envoyerRequete = () => {
    const requete = new Requete;
    requete.message = this.requeteForm.value.message;

    this.requeteService.saveRequete(requete).subscribe(
      () => {},
      err => {console.warn(err); });
}

}
