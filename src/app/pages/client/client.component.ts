import { Component, OnInit } from '@angular/core';
import { ClientHttpService } from 'src/app/services/client/client-http.service';
import { Client } from 'src/app/models/client/client';
import { TypeRequete } from 'src/app/models/typeRequete/type-requete';
import { TypeRequeteHttpService } from 'src/app/services/typeRequete/type-requete-http.service';
import { CompteHttpService } from 'src/app/services/compte/compte-http.service';
import { Compte } from 'src/app/models/compte/compte';
import { TransactionHttpService } from 'src/app/services/transaction/transaction-http.service';
import { Transaction } from 'src/app/models/transaction/transaction';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {
  client: Client;
  typeRequetes: TypeRequete[];
  comptes: Compte[];
  transactions: Transaction[];
  constructor(private clientService: ClientHttpService,
              private typeRequeteService: TypeRequeteHttpService,
              private compteService: CompteHttpService,
              private transactionService: TransactionHttpService
              ) { }

  ngOnInit() {
    // Code pour relancer la page à l'ouverture de la page
    const urlParams = [];
    // tslint:disable-next-line: only-arrow-functions
    window.location.search.replace('?', '').split('&').forEach(function(e, i) {
        const p = e.split('=');
        urlParams[p[0]] = p[1];
    });

    // We have all the params now -> you can access it by name
    console.log(urlParams['loaded']);

    if (urlParams['loaded']) {} else{

        const win = (window as any);
        win.location.search = '?loaded=1';
    }

    // tslint:disable-next-line: radix
    this.clientService.getClientByIdUtilisateur(parseInt(localStorage.getItem('idUtilisateur'))).subscribe(
      (client: Client) => {
        this.client = client;
        console.log(client); },
      err => {console.warn(err); }
    );
    this.typeRequeteService.getTypesRequetes().subscribe(
      (typeRequetes: TypeRequete[]) => {
        this.typeRequetes = typeRequetes;
        console.log(typeRequetes); },
      err => {console.warn(err); }
    );
    // tslint:disable-next-line: radix
    this.compteService.getComptesByIdClient(parseInt(localStorage.getItem('idClient'))).subscribe(
      (comptes: Compte[]) => {
        this.comptes = comptes;
        console.log(comptes); },
      err => {console.warn(err); }
    );
  }

}
