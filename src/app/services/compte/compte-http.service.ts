import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CompteHttpService {
  private url: string = 'http://localhost:8080/comptes';

  constructor(private http: HttpClient) { }

  getComptes = () => {
    return this.http.get(this.url);
  }

  getComptesByIdClient = (id: number) => {
    return this.http.get(this.url + `/client` + `/${id}`);
  }
}
