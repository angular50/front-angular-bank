import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurHttpService {
  private url: string = 'http://localhost:8080/utilisateurs';

  constructor(private http: HttpClient) { }

  getUtilisateurs = () => {
    return this.http.get(this.url);
  }
}
