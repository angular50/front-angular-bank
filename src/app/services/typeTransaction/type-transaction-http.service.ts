import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TypeTransactionHttpService {
  private url: string = 'http://localhost:8080/typesTransactions';

  constructor(private http: HttpClient) { }

  getTypesTransactions = () => {
    return this.http.get(this.url);
  }
}
