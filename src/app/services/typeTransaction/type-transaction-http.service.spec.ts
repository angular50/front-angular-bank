import { TestBed } from '@angular/core/testing';

import { TypeTransactionHttpService } from './type-transaction-http.service';

describe('TypeTransactionHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypeTransactionHttpService = TestBed.get(TypeTransactionHttpService);
    expect(service).toBeTruthy();
  });
});
