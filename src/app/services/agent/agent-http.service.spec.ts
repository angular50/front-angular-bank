import { TestBed } from '@angular/core/testing';

import { AgentHttpService } from './agent-http.service';

describe('AgentHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AgentHttpService = TestBed.get(AgentHttpService);
    expect(service).toBeTruthy();
  });
});
