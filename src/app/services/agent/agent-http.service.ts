import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AgentHttpService {
  private url: string = 'http://localhost:8080/agents';

  constructor(private http: HttpClient) { }

  getAgents = () => {
    return this.http.get(this.url);
  }
}
