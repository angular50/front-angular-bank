import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NotificationHttpService {
  private url: string = 'http://localhost:8080/notifications';

  constructor(private http: HttpClient) { }

  getNotifications = () => {
    return this.http.get(this.url);
  }
}
