import { TestBed } from '@angular/core/testing';

import { TypeRequeteHttpService } from './type-requete-http.service';

describe('TypeRequeteHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypeRequeteHttpService = TestBed.get(TypeRequeteHttpService);
    expect(service).toBeTruthy();
  });
});
