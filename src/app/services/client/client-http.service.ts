import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClientHttpService {
  private url: string = 'http://localhost:8080/clients';

  constructor(private http: HttpClient) { }

  getClients = () => {
    return this.http.get(this.url);
  }

  getClientByIdUtilisateur = (id: number) => {
    return this.http.get(this.url + `/utilisateur` + `/${id}`);
  }
}
