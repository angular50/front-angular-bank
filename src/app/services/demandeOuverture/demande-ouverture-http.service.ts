import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DemandeOuvertureHttpService {
  private url: string = 'http://localhost:8080/demandesOuvertures';

  constructor(private http: HttpClient) { }

  getDemandes = () => {
    return this.http.get(this.url);
  }
}
