import { TestBed } from '@angular/core/testing';

import { DemandeOuvertureHttpService } from './demande-ouverture-http.service';

describe('DemandeOuvertureHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DemandeOuvertureHttpService = TestBed.get(DemandeOuvertureHttpService);
    expect(service).toBeTruthy();
  });
});
