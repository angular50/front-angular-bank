import { Injectable } from '@angular/core';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {
  private url:string = "http://localhost:8080/utilisateurs/login";
  constructor(private http: HttpClient) { }

  authentification(utilisateur: Utilisateur){
    return this.http.post(this.url, utilisateur);
  }

}
