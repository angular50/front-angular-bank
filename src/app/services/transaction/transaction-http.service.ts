import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TransactionHttpService {
  private url: string = 'http://localhost:8080/transactions';

  constructor(private http: HttpClient) { }

  getTransactions = () => {
    return this.http.get(this.url);
  }

  getTransactionsByIdCompte = (id: number) => {
    return this.http.get(this.url + `/compte` + `/${id}`);
  }
}
