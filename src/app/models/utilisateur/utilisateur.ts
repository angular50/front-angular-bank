export class Utilisateur {
  idutilisateur: number;
  nom: string;
  prenom: string;
  email: string;
  adresse: string;
  telephone: string;
  pseudo: string;
  mdp: string;
}
