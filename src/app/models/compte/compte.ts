import { Client } from '../client/client';

export class Compte {
  idcompte: number;
  numCompte: string;
  rib: string;
  solde: number;
  decouvert: number;
  montantAgios: number;
  seuilRemuneration: number;
  montantRemuneration: number;
  client: Client = new Client();
}
