import { Client } from '../client/client';

export class Requete {
  idrequete: number;
  message: string;
  date: Date;
  client: Client = new Client();
}
