import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
export class Admin {
  idadmin: number;
  matricule: string;
  utilisateur: Utilisateur = new Utilisateur();
}
