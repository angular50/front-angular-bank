import { Compte } from '../compte/compte';

export class Notif {
  idnotification: number;
  date: Date;
  message: string;
  compte: Compte = new Compte();
}
