import { Compte } from '../compte/compte';

export class Transaction {
  idtransaction: number;
  montant: number;
  libelle: string;
  date: Date;
  compte: Compte = new Compte();
}
