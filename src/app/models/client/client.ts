import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { Agent } from './../agent/agent';
export class Client {
  idclient: number;
  identifiant: string;
  piecesJustif: string;
  revenuMensuel: number;
  agent: Agent = new Agent();
  utilisateur: Utilisateur = new Utilisateur();
}
