export class ClientPotentiel {
  idclientPotentiel: number;
  nom: string;
  prenom: string;
  email: string;
  adresse: string;
  telephone: string;
  revenuMensuel: number;
  piecesJustif: string;
}
