import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { Admin } from '../admin/admin';
export class Agent {
  idagent: number;
  matricule: string;
  utilisateur: Utilisateur = new Utilisateur();
  admin: Admin = new Admin();
}
